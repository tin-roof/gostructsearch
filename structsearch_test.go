package gss

import (
	"fmt"
	"testing"
)

type base struct {
	ID          int
	Description description
	List        []string
}

type description struct {
	Color    string
	Size     int
	Location location
}

type location struct {
	Latitude  float64
	Longitude float64
}

func TestTopLevel(t *testing.T) {
	// Define the data
	var data base = base{
		ID: 100,
		Description: description{
			Color: "yellow",
			Size:  250,
			Location: location{
				Latitude:  1.29,
				Longitude: 2.39,
			},
		},
		List: []string{"one", "two"},
	}

	// Search the struct
	value, _ := Search(data, "ID")

	// Print the result
	fmt.Println(value)

	// Output: 100
}

func TestSecondLevel(t *testing.T) {
	// Define the data
	var data base = base{
		ID: 100,
		Description: description{
			Color: "yellow",
			Size:  250,
			Location: location{
				Latitude:  1.29,
				Longitude: 2.39,
			},
		},
		List: []string{"one", "two"},
	}

	// Search the struct
	value, _ := Search(data, "Description.Color")

	// Print the result
	fmt.Println(value)

	// Output: yellow
}

func TestThirdLevel(t *testing.T) {
	// Define the data
	var data base = base{
		ID: 100,
		Description: description{
			Color: "yellow",
			Size:  250,
			Location: location{
				Latitude:  1.29,
				Longitude: 2.39,
			},
		},
		List: []string{"one", "two"},
	}

	// Search the struct
	value, _ := Search(data, "Description.Location.Latitude")

	// Print the result
	fmt.Println(value)

	// Output: 1.29
}

func TestPathDoesntExist(t *testing.T) {
	// Define the data
	var data base = base{
		ID: 100,
		Description: description{
			Color: "yellow",
			Size:  250,
			Location: location{
				Latitude:  1.29,
				Longitude: 2.39,
			},
		},
		List: []string{"one", "two"},
	}

	// Search the struct
	_, err := Search(data, "Description.Color.Base")

	// Print the result
	fmt.Println(err.Error())

	// Output: path does not exist
}
