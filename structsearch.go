package gss

import (
	"errors"
	"reflect"
	"strings"
)

// Search find the value of the key at the given path inside a struct
func Search(object interface{}, path string) (interface{}, error) {
	// Split the path
	var parts = strings.Split(path, ".")

	// Call the search function
	return search(object, parts)
}

// search recursive search function that goes until the path is empty and returns the value or error
func search(object interface{}, path []string) (value interface{}, err error) {
	// Make the reflection
	reflection := indirect(reflect.ValueOf(object))

	// Find the field
	field := reflection.FieldByName(path[0])
	defer func() {
		// Set the error if this is a recover operation
		if r := recover(); r != nil {
			err = errors.New("path does not exist")
		}
	}()

	// Path is empty return what is in the field
	if len(path) <= 1 {
		return field.Interface(), nil
	}

	// Recursively call the search stripping the first path part out
	return search(field.Interface(), path[1:])
}

// indirect get the indrect value if its a pointer
func indirect(reflectValue reflect.Value) reflect.Value {
	for reflectValue.Kind() == reflect.Ptr {
		reflectValue = reflectValue.Elem()
	}
	return reflectValue
}
