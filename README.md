# goStructSearch

Package to allow searching a struct by . notation

## Usage

```go
package main

import (
    gss "gitlab.com/tin-roof/gostructsearch"
)

// StructToSearch create the type of the struct to search
type StructToSearch struct {
    TestKey string
    TestNested NestedStruct
}

// NestedStruct create the type for the nested struct
type NestedStruct struct {
    TestNestedKey string
}

func main() {
    // Creating the struct
    var newStruct StructToSearch = StructToSearch{
        TestKey: "TestingValue"
        TestNested NestedStruct {
            TestNestedKey "TestingNestedValue"
        }
    }
    
    // Get value at top level
    value1, err1 := gss.Search(newStruct, "TestKey")
    fmt.Printf("Value: %v", value1) // Value: TestingValue
    
    // Get value at nested key
    value2, err2 := gss.Search(newStruct, "TestNested.TestNestedKey")
    fmt.Printf("Value: %v", value2) // Value: TestingNestedValue
    
    // Get value at path that doesn't exist
    value3, err3 := gss.Search(newStruct, "TestNested.DoesntExist")
    fmt.Printf("Value: %v", value3) // Value: nil
    if err3 != nil {
        fmt.Printf("Error: %s", err3.Error()) // Error: path does not exist
    }
    
}
```